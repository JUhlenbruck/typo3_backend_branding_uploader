<?php
/**
 * Created by PhpStorm.
 * User: januhlenbruck
 * Date: 18.08.16
 * Time: 11:23
 */

include ("databaseConnect.php");

$AlleKunden = mysqli_query($db, "SELECT * FROM kunden_informationen");

$AlleKundenList = $AlleKunden->fetch_all(MYSQLI_ASSOC);

foreach ($AlleKundenList as $EinKundeList) {
    ?>
        <script>
            $('#einstellungBtn<?php echo $EinKundeList['uID']; ?>').on('click', function ( event ) {
                event.preventDefault();
                var kdID = <?php echo $EinKundeList['uID']; ?>;
                var ajaxKundenDatenAntwort = $.ajax({
                    method: 'POST',
                    url: "php/kundenDatenBearbeiten.php",
                    data: {
                        kdID: kdID
                    },
                    success: function () {
                        $('#modalBodyKundenEinstellungen').empty();
                        $('#modalBodyKundenEinstellungen').append( ajaxKundenDatenAntwort.responseText );
                    }
                });
            });
        </script>
        <span class="btn btn-info kundenBtn" data-kdID="<?php echo $EinKundeList['uID']; ?>" data-ident="<?php echo $EinKundeList['ident']; ?>" title="<?php echo $EinKundeList['ident']; ?> bearbeiten" id="einstellungBtn<?php echo $EinKundeList['uID']; ?>">
            <?php echo $EinKundeList['ident']; ?>
        </span>


<?php
}

mysqli_close($db);