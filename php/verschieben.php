<?php


include("databaseConnect.php");


$aktuellePosition = $_POST['aktuellePosition'] - 1 + 1;
$neuePositionLinks = $aktuellePosition - 1;
$neuePositiomRechts = $aktuellePosition + 1;
$verschiebungsArt = $_POST['verschiebung'];

$BilderMaxPosition = mysqli_query($db, "SELECT MAX(position) AS maxposition FROM bild_informationen");

$bilderMaxPositiom = $BilderMaxPosition->fetch_assoc();

$maxPosition = $bilderMaxPositiom["maxposition"];

if ($aktuellePosition > 0 && $verschiebungsArt == "links") {

    $ZuVerschiebenesBild = mysqli_query($db, "SELECT * FROM bild_informationen WHERE position =" . $aktuellePosition);
    $zuVerscheibenesBild = $ZuVerschiebenesBild->fetch_assoc();


    $LinksDaneben = mysqli_query($db, "SELECT * FROM bild_informationen WHERE position =" . $neuePositionLinks);
    $linksDaneben = $LinksDaneben->fetch_assoc();

    mysqli_query($db, "UPDATE bild_informationen SET position=" . $neuePositionLinks . " WHERE uID =" . $zuVerscheibenesBild['uID']);
    mysqli_query($db, "UPDATE bild_informationen SET position=" . $aktuellePosition . " WHERE uID =" . $linksDaneben['uID']);
}

if ($aktuellePosition < $maxPosition && $verschiebungsArt == "rechts") {

    $ZuVerschiebenesBild = mysqli_query($db, "SELECT * FROM bild_informationen WHERE position =" . $aktuellePosition);
    $zuVerscheibenesBild = $ZuVerschiebenesBild->fetch_assoc();


    $LinksDaneben = mysqli_query($db, "SELECT * FROM bild_informationen WHERE position =" . $neuePositiomRechts);
    $linksDaneben = $LinksDaneben->fetch_assoc();

    mysqli_query($db, "UPDATE bild_informationen SET position=" . $neuePositiomRechts . " WHERE uID =" . $zuVerscheibenesBild['uID']);
    mysqli_query($db, "UPDATE bild_informationen SET position=" . $aktuellePosition . " WHERE uID =" . $linksDaneben['uID']);
}

mysqli_close($db);