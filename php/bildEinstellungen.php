<?php
/**
 * Created by PhpStorm.
 * User: januhlenbruck
 * Date: 17.08.16
 * Time: 13:05
 */

$bildID = $_POST['bildID'];

include ("databaseConnect.php");

//alle Kunden die dieses Bild sehen ermitteln

$SehendeKundenID = mysqli_query( $db , "SELECT kdID FROM bilder_kunden WHERE bildID =" . $bildID);

$SehendeKundenIDList = $SehendeKundenID->fetch_all(MYSQLI_ASSOC);

foreach ( $SehendeKundenIDList as $kundenID) {
    $sehendeKundenIDList[] = $kundenID;
}



//alle Kunden die eingetragen sind

$AlleKunden = mysqli_query( $db, "SELECT * FROM kunden_informationen");

$AlleKundenList = $AlleKunden->fetch_all(MYSQLI_ASSOC);


foreach ($AlleKundenList as $einKunde){

    ?>
    <script>

        $('#btn-group<?php echo $einKunde['uID']; ?>').on('click', function ( event ) {
            event.preventDefault();
            var checkValue = $("#check<?php echo $einKunde['uID']; ?>")["0"].checked;
            if(checkValue){
                $('#check<?php echo $einKunde['uID']; ?>').prop( 'checked', false );
            }

            else {
                $('#check<?php echo $einKunde['uID']; ?>').prop( 'checked', true );
            }

        });

        //TODO: Warum klappt das?
        $('#check<?php echo $einKunde['uID']; ?>').on('click', function ( event ) {

            event.stopPropagation();
            var checkValue = $("#check<?php echo $einKunde['uID']; ?>")["0"].checked;

            if(!checkValue){
                $('#check<?php echo $einKunde['uID']; ?>').prop( 'checked', false );
            }

            else {
                $('#check<?php echo $einKunde['uID']; ?>').prop( 'checked', true );
            }
        });

    </script>
    <div class="btn-group " id="btn-group<?php echo $einKunde['uID']; ?>">
        <span class="btn btn-danger kundenBtn" data-kdID="<?php echo $einKunde['uID']; ?>" data-name="<?php echo $einKunde['ident']; ?>" id="span<?php echo $einKunde['uID']; ?>">
        <input type="checkbox" class="btn btn-danger sizeing sehendeKunden" id="check<?php echo $einKunde['uID']; ?>"

            <?php
            foreach ($SehendeKundenIDList as $sehenderKunde) {
                if ($einKunde['uID'] ==  $sehenderKunde['kdID']) {
                    echo "checked";
                }
            }
            ?>
               > <?php echo $einKunde['ident']; ?>
            </span>



    </div>

<?php
}
$AlleKundenID = mysqli_query( $db, "SELECT uID FROM kunden_informationen");

$AlleKundenIDList = $AlleKundenID->fetch_all(MYSQLI_ASSOC);


foreach ($AlleKundenIDList as $einKundeID){
    foreach ($einKundeID as $kundenID) {
        $kundenIDList[] = $kundenID;
    }
}

?>

<script>
    $('#speichern').on('click', function ( event) {
        event.preventDefault();
        var sehendeKunden = [];
        var bildID = <?php echo $bildID ?>;
        <?php
        foreach ($kundenIDList as $kundeID){
        ?>
        var checkValue = $("#check<?php echo $kundeID; ?>")["0"].checked;
        if ( checkValue) {
            sehendeKunden[<?php echo $kundeID; ?>] = 1;
        }
        else {
            sehendeKunden[<?php echo $kundeID; ?>] = 0;
        }
        <?php } ?>

        $.ajax({
            method: 'POST',
            url: '/php/sichtbarkeitsAenderung.php',
            data: {
                sehendeKunden: sehendeKunden,
                bild: bildID
                },
            success: function (data) {
                window.location.reload();
            }
            });
        });
    $('#einstellungSchließen').on('click', function ( event ){
        event.preventDefault();
        window.location.reload();
    });



</script>
<?php

mysqli_close($db);