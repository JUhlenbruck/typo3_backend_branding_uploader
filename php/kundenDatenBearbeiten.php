<?php
/**
 * Created by PhpStorm.
 * User: januhlenbruck
 * Date: 18.08.16
 * Time: 11:01
 */


include("databaseConnect.php");

$kdID = $_POST['kdID'];

if ($kdID) {


    $KundeOption = mysqli_query($db, "SELECT * FROM kunden_informationen WHERE uID =" . $kdID);
    $KundeOptionList = $KundeOption->fetch_assoc();


    $kdName = $KundeOptionList['kundenname'];
    $kdIdent = $KundeOptionList['ident'];
    $kdID = $KundeOptionList['uID'];

    ?>
    <div data-id="<?php echo $kdID ?>" id="kunden">
        <div class="btn-group">
            <lable class="btn btn-primary size">
                Kundenname:
            </lable>
            <input type="text" name="kdname" id="kdName" class="btn btn-info size" value="<?php echo $kdName; ?>">
        </div>
        <br/>
        <div class="btn-group">
            <lable class="btn btn-primary size">
                Kundenidentifikation:
            </lable>
            <input type="text" name="kdIdent" id="kdIdent" class="btn btn-info size" value="<?php echo $kdIdent; ?>">
        </div>
        <button id="kundenEntfernen" title="Kunde löschen" class="btn btn-danger glyphicon glyphicon-erase ">

        </button>

        <button class="btn btn-success" title="Änderungen speichern" id="aenderungenSpeichern">
            Änderungen Speichern
        </button>
    </div>

    <script>
        $('#aenderungenSpeichern').on('click', function (event) {
            event.preventDefault();
            var kdName = $('#kdName')["0"].value;
            var kdIdent = $('#kdIdent')["0"].value;
            var kdID = $('#kunden').data('id');

            $.ajax({
                method: 'POST',
                url: "php/kundenDatenSpeichern.php",
                data: {
                    kdName: kdName,
                    kdIdent: kdIdent,
                    kdID: kdID
                },
                 complete: function () {
                 window.location.reload();
                 }
            });
        });

        $('#kundenEntfernen').on('click', function ( event ) {
            var kdID =  $('#kunden').data('id');

            $.ajax({
                method: 'POST',
                url: "php/kundeEntfernen.php",
                data: {
                    ID: kdID
                },
                complete: function () {
                    window.location.reload();
                }
            });
        });
    </script>


<?php }
else{
    $kdName = "Kundennamen";
    $kdIdent = "Kundenidentifikation";
    $kdID = null
    ?>
    <div data-id="<?php echo $kdID ?>" id="kunden">
        <div class="btn-group">
            <lable class="btn btn-primary size">
                Kundenname:
            </lable>
            <input type="text" name="kdname" id="kdName" class="btn btn-info size" value="<?php echo $kdName; ?>">
        </div>
        <br/>
        <div class="btn-group">
            <lable class="btn btn-primary size">
                Kundenidentifikation:
            </lable>
            <input type="text" name="kdIdent" id="kdIdent" class="btn btn-info size" value="<?php echo $kdIdent; ?>">
        </div>

        <button class="btn btn-success" id="aenderungenSpeichern">
            Speichern
        </button>
    </div>


    <script>
        $('#aenderungenSpeichern').on('click', function (event) {
            event.preventDefault();
            var kdName = $('#kdName')["0"].value;
            var kdIdent = $('#kdIdent')["0"].value;
            var kdID = $('#kunden').data('id');

            $.ajax({
                method: 'POST',
                url: "php/kundenDatenSpeichern.php",
                data: {
                    kdName: kdName,
                    kdIdent: kdIdent
                },
                 complete: function () {
                 window.location.reload();
                 }
            });
        });
    </script>

    <?php


}



?>



<?php
mysqli_close($db);
