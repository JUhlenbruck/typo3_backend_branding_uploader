<?php

include("databaseConnect.php");

$target_path = "../upload/";                                           // Uploaf-Pfad

$i = 0;
$bildname = $_FILES['upload']['name'];                             //Bildname
$beschreibung = $_POST["beschreibung"];                             //Beschreibung
$groesse = ($_FILES['upload']['size'] / 1024);                        //Datei größe
$dateiFormat = $_FILES['upload']['type'];                           //Format der Datei
$uploadDatum = date("Y-m-d H:i:s");                                 //Datum des Uploads


$BilderZahl = mysqli_query($db, "SELECT COUNT(bildname) AS anzahl FROM bild_informationen");

$bilderAnzahl = $BilderZahl->fetch_assoc();

$neuePosition = $bilderAnzahl["anzahl"];

$dateiInfo = pathinfo($bildname);

while (file_exists($target_path . $bildname)) {
    $bildname = $dateiInfo['filename'] . $i . "." . $dateiInfo['extension'];
    $i += 1;
};

$SQLeinfügen = "INSERT INTO bild_informationen 
                        (bildname,      beschreibung,      groesse,     position,    uploadDatum,    dateiformat) 
                VALUES  ('$bildname',  '$beschreibung',   '$groesse',  '$neuePosition', '$uploadDatum', '$dateiFormat')";

$target_path = $target_path . $bildname; //Dateipfad auf dem Server

if ($dateiFormat == "image/jpeg" || $dateiFormat == "image/png" || $dateiFormat == "image/gif") {

    if (move_uploaded_file($_FILES['upload']['tmp_name'], $target_path)) {

        mysqli_query($db, $SQLeinfügen);

        echo "Die Datei " . basename($_FILES['upload']['name']) . " wurde als " . $bildname . " hochgeladen.";

    } else {
        echo "Es gab ein Problem, bitte versuch es erneut!";
    }
}

mysqli_close($db);