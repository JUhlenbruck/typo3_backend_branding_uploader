<?php

include('databaseConnect.php');            //Datenbankverbindung aufbauen


$dateiURL = $_POST['link'];                 //URL der zu löschenden Datei
$dateiName = $_POST['name'];                //Name der zu löschenden Datei
$dateiPosition = $_POST['position'];        //Position der Datei in der Anzeige
$bildID = $_POST['bildID'];

$SQLanfrage = "DELETE FROM bild_informationen WHERE uID =" . $bildID;

unlink("../upload/" . $dateiName);                          //Datei löschen
mysqli_query($db, $SQLanfrage);                             //Datenbankeintrag löschen

$AlleBildnamen = mysqli_query($db, "SELECT * FROM bild_informationen WHERE position >" . $dateiPosition);             //ein Objekt mit dem Ergebnis der Anfrage

$alleBildnamenList = $AlleBildnamen->fetch_all(MYSQLI_ASSOC);                                                         //beinhaltet das Array aus dem Objelt

foreach ($alleBildnamenList as $bild) {
    $einBildList [] = $bild;
}

foreach ($einBildList as $bewegeBild) {
    $neuePosition = $bewegeBild['position'] - 1;
    mysqli_query($db, "UPDATE bild_informationen SET position=" . $neuePosition . " WHERE uID = " . $bewegeBild['uID']);
}

if (file_exists("upload/" . $dateiName)) {
    echo "Ein Fehler ist aufgetreten";
} else {
    echo "Datei erfolgreich gelöscht!";
    mysqli_query($db, "DELETE FROM bilder_kunden WHERE bildID=" . $bildID);
}
mysqli_close($db);