<?php

include('databaseConnect.php');


$dirName = "/upload/";

$AlleBildnamen = mysqli_query($db, "SELECT * FROM bild_informationen ORDER BY position");             //ein Objekt mit dem Ergebnis der Anfrage

$alleBildnamenList = $AlleBildnamen->fetch_all(MYSQLI_ASSOC);                                         //beinhaltet das Array aus dem Objelt

foreach ($alleBildnamenList as $bild) {
    $einBildList [] = $bild;
}


$BilderMaxPosition = mysqli_query($db, "SELECT MAX(position) AS maxposition FROM bild_informationen");

$bilderMaxPositiom = $BilderMaxPosition->fetch_assoc();

$maxPosition = $bilderMaxPositiom["maxposition"];

mysqli_close($db);

foreach ($einBildList as $bildInfo) {

    ?>

    <div class="grid-item  col-sm-6 col-md-4" data-uid="<?php echo $bildInfo['uID']; ?>">

        <div class=" gleich thumbnail">
            <button type="button" class="btn btn-primary modalButtonEinstellungen glyphicon glyphicon-cog" title="Sichtbarkeit ändern" data-toggle="modal"
                   data-target="#myModalEinstellungen" data-bildid="<?php echo $bildInfo['uID']; ?>">
            </button>
            <img src="<?php echo $dirName . $bildInfo['bildname']; ?>" title="<?php echo $bildInfo['bildname']; ?>" alt="<?php echo $bildInfo['bildname']; ?>">

            <div class="caption">
                <h3>
                    <?php echo $bildInfo['bildname']; ?>
                </h3>

                <p class="beschreibung text-center">
                    <?php echo $bildInfo['beschreibung']; ?>
                </p>

                <p class="text-center">
                    Hochgeladen am: <?php echo $bildInfo['uploadDatum']; ?>
                </p>
            </div>


            <div class="btn-group" role="group">

                <button class="btn btn-primary nachlinks verschieben glyphicon glyphicon-chevron-left"
                        title="Nach links verschieben"
                        data-position="<?php echo $bildInfo['position']; ?>"
                        data-verschiebung="links">
                    <?php if ($bildInfo['position'] == 0) {
                        echo "";
                    } else {
                        echo "";
                    }
                    ?>
                </button>


                <label class="btn btn-primary " title="Aktuelle Position">
                    <p class=<?php echo "position" . ($bildInfo['position'] + 1); ?>>

                    </p>
                </label>


                <button class="btn btn-primary nachrechts verschieben glyphicon glyphicon-chevron-right"
                        title="Nach rechts verschieben"
                        data-position="<?php echo $bildInfo['position']; ?>"
                        data-verschiebung="rechts">
                    <?php if ($bildInfo['position'] == $maxPosition) {
                        echo "";
                    } else {
                        echo "";
                    }
                    ?>
                </button>
            </div>

            <div class="btn-group" role="group" id="löschenÄndern">
                <button data-url="<?php echo $dirName . $bildInfo['bildname']; ?>"
                        data-id="<?php echo $bildInfo['uID']; ?>"
                        data-name="<?php echo $bildInfo['bildname']; ?>"
                        title="Beschreibung ändern"
                        class="btn btn-primary ändern">
                    Beschreibung ändern
                </button>
                <button data-url="<?php echo $dirName . $bildInfo['bildname']; ?>"
                        data-id="<?php echo $bildInfo['uID']; ?>"
                        data-position="<?php echo $bildInfo['position']; ?>"
                        data-name="<?php echo $bildInfo['bildname']; ?>"
                        title="Bild löschen"
                        class="btn btn-primary loeschen">
                    Löschen
                </button>
            </div>


        </div>
    </div>



    <?php
}
?>



