var gulp = require('gulp'),
    less = require('gulp-less'),
    watch = require('gulp-watch');

var project = {
    styles: {
        mainTemplate:  "less/bootstrap.less",   // file with includes (e.g. template.less)
        lessDirectory: "less/**/*.less",        // less main directory
        destDirectory: "css/",         // directory for compiled css files
    }
};

/*
 * Task to watch less file changes
 * Executes 'compile-less' task
 */
gulp.task('watch-less', function() {
    gulp.watch(project.styles.lessDirectory , ['compile-less']);
});

/*
 * Task to compile less to css
 */
gulp.task('compile-less', function() {
    gulp.src(project.styles.mainTemplate)
        .pipe(less())
        .pipe(gulp.dest(project.styles.destDirectory));
});

/*
 * Tasklist
 */
gulp.task('default', ['watch-less', 'compile-less']);