<!DOCTYPE html>
<html lang="en">
<head>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--<script src="js/callJsonApi.js"></script>-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>backend branding upload</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- packery & dragabilly -->
    <script src="js/packery.js"></script>
    <script src="js/draggabilly.js"></script>


    <script>
        $(document).ready(function () {

            // initialize Packery
            var $grid = $('.grid').packery({
                itemSelector: '.grid-item',
                // columnWidth helps with drop positioning
                columnWidth: 380,
                rowHeight: 500
            });

            // make all grid-items draggable
            $grid.find('.grid-item').each(function (i, gridItem) {
                var draggie = new Draggabilly(gridItem);

                // bind drag events to Packery
                $grid.packery('bindDraggabillyEvents', draggie);
            });

            orderItems();

            // show item order after layout
            function orderItems() {
                var itemElems = $grid.packery('getItemElements');
                $(itemElems).each(function (i, itemElem) {
                    $(itemElem).children().children('div').children(1).children(4).text(i + 1);
                });
            }

            //Datenbankeinträge aktualisieren
            function elementSortieren() {
                var BilderInformationenList = [];
                var itemElems = $grid.packery('getItemElements');
                $(itemElems).each(function (i, itemElem) {
                    var bilduID = $(itemElem).data('uid');
                    BilderInformationenList[i] = bilduID;
                });

                //neu sortieren
                $.ajax({
                    type: "POST",
                    url: "php/neuSortieren.php",
                    data: {
                        informationen: BilderInformationenList
                    }
                });
            }

            //zurechtrücken von fehlplazierten Elementen
            function fillGap() {
                setTimeout(function () {
                    $grid.packery();
                }, 100);
            }

            $grid.on('dragItemPositioned', elementSortieren);
            $grid.on('layoutComplete', orderItems);
            $grid.on('dragItemPositioned', orderItems);
            $grid.on('dragItemPositioned', fillGap);


            //Datei löschen
            $(".loeschen").on("click", function (event) {
                event.preventDefault();

                var url = $(this).data('url');
                var bildName = $(this).data('name');
                var bildPosition = $(this).data('position');
                var bildID = $(this).data('id');
                $.ajax({
                    method: "POST",
                    url: "php/bild.del.php",
                    data: {
                        name: bildName,
                        link: url,
                        position: bildPosition,
                        bildID: bildID
                    },
                    error: function (i, e, f) {
                        console.log(i, e, f);
                    },
                    complete: function () {
                        window.location.reload();
                    }
                });
            });

            //Datei hochladen
            $('#hochladen').on('click', function (event) {

                event.preventDefault();
                var data = new FormData();
                $.each($('input[type="file"]')[0].files, function (i, file) {
                    data.append('upload', file);
                });

                var textFieldInhalt = $('#beschreibung').val();

                data.append('beschreibung', textFieldInhalt);

                $.ajax({
                    url: "php/upload.php",
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        window.location.reload();
                    }
                });
            });

            //Dateiname anzeigen
            $('#dateiWahl').on('change', function () {

                var dateiName = $(this)[0].files[0].name;
                $('#dateiBezeichnung').text(dateiName);
            });

            //trigger #hochladen
            $('#hochladenErsatz').on('click', function (event) {
                event.preventDefault();
                $('#hochladen').trigger('click', true);
            });

            //trigger #dateiWahl
            $('#dateiBezeichnung').on('click', function (event) {
                event.preventDefault();
                $('#dateiWahl').trigger('click', true);
            });

            //Dateiinformationen ändern
            $('.ändern').on('click', function (event) {
                event.preventDefault();

                var bildName = $(this).data('name');
                var bildID = $(this).data('id');
                var ergebnisText = prompt("Bitte neue Beschreibung eingeben.", "Maximal 255 Zeichen.");

                $.ajax({
                    method: "POST",
                    url: "php/beschreibungAendern.php",
                    cache: false,
                    data: {
                        beschreibung: ergebnisText,
                        name: bildName,
                        bildID: bildID
                    },
                    complete: function () {
                        window.location.reload();
                    }
                });
            });

            //Verschieben
            $('.verschieben').on('click', function (event) {
                event.preventDefault();

                var position = $(this).data('position');
                var verschiebung = $(this).data('verschiebung');

                $.ajax({
                    method: "POST",
                    url: "php/verschieben.php",
                    cache: false,
                    data: {
                        aktuellePosition: position,
                        verschiebung: verschiebung
                    },
                    complete: function () {
                        window.location.reload();
                    }
                });
            });

            //Beispiel
            $('.beispiel').on('click', function (event) {
                event.preventDefault();
                window.open("/beispielSeite/index.html");
            });

            //Einstellungen öffnen
            $('.modalButtonEinstellungen').on('click', function (){
                var ID = $(this).data('bildid');
                $('#modalBodyEinstellungen').empty();
                    var ajaxAntwort = $.ajax({
                       method: 'POST',
                        url: "php/bildEinstellungen.php",
                        data: {
                           bildID: ID
                        },
                        success: function () {
                            $('#modalBodyEinstellungen').append(
                                ajaxAntwort.responseText);
                        }
                    });
            });


            //Kunden Einstellungen öffnen
            $('#modalKundenButton').on('click', function ( event ) {
                $('#modalBodyKundenEinstellungen').empty();
                var ajaxAntwort = $.ajax({
                    method: 'POST',
                    url: "php/kundenAbrufen.php",
                    success: function () {
                        $('#modalBodyKundenEinstellungen').append(
                            ajaxAntwort.responseText);
                    }

                });
            });

            //Kunde Hinzufügen
            $('#kundenHinzufuegen').on('click', function ( event ) {
                event.preventDefault();
                var kdID = null;
                var ajaxKundenDatenAntwort = $.ajax({
                    method: 'POST',
                    url: "php/kundenDatenBearbeiten.php",
                    data: {
                        kdID: kdID
                    },
                    success: function () {
                        $('#modalBodyKundenEinstellungen').empty();
                        $('#modalBodyKundenEinstellungen').append( ajaxKundenDatenAntwort.responseText );
                    }
                });
            });


        });

    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<!-- Gallery div für Hintergrundbilder -->
<div id="gallery"></div>

<!-- div where items are dragable -->
<div class="grid ">
    <!-- Button trigger modal -->
    <label type="button" id="modalButton" class="btn btn-primary btn-lg glyphicon glyphicon-plus" data-toggle="modal"
           data-target="#myModal" title="Bild hinzufügen">
    </label>

    <button class="btn btn-success beispiel glyphicon glyphicon-play margin-top-10" title="Beispielseite anzeigen">
    </button>

    <label type="button" id="modalKundenButton" class="btn btn-info btn-lg glyphicon glyphicon-cog" data-toggle="modal" title="Kundeneinstellungen"
           data-target="#myModalKundenEinstellungen">
    </label>

    <?php include('php/bilderLaden.php') ?>
</div>

<!-- Modal zum uploaden von Dateien -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

                <h4 class="modal-title" id="myModalLabel">Datei hinzufügen</h4>

            </div>

            <div class="modal-body">
                <form action="php/upload.php" method="post" enctype="multipart/form-data" class="">
                    <div class="btn-group">
                        <label class="btn btn-primary btn-file margin-top-4"> Datei auswählen
                            <input type="file" name="datei" class="hidden" id="dateiWahl">
                        </label>

                        <label class="btn btn-primary sizeing margin-top-10" id="dateiBezeichnung">Keine Datei
                            gewählt</label>

                    </div>

                    <hr/>

                    <label name="beschreibung">
                        Kurze Beschreibung des Bildes:
                    </label>
                    <br/>
                    <textarea rows="6" cols="50" name="beschreibung" id="beschreibung">Kurze Beschreibung für das Bild. Maximal 255 Zeichen! Akzeptierte Dateiformate sind .jpg, .gif und .png.</textarea>

                    <br/>

                    <input type="submit" class="btn btn-default btn-file margin-top-10 invisible" name="upload"
                           id="hochladen" value="Hochladen">

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" id="hochladenErsatz" class="btn btn-success">Hochladen</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Abbrechen</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal für Sichtbarkeitseinstellungen -->

<div class="modal fade" id="myModalEinstellungen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="close einstellungSchließen" data-dismiss="modal" aria-label="Close"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <div id="myModalEinstellungenLabel">
                    <h4 class="modal-title" id="myModalLabelEinstellungen">Sichtbarkeitseinstellungen</h4>

                </div>

            </div>

            <div class="modal-body" id="modalBodyEinstellungen">

            </div>
            <div class="modal-footer">
                <button type="button" id="speichern" class="btn btn-success einstellungSchließen">Speichern</button>
                <button type="button" id="einstellungSchließen" class="btn btn-danger einstellungSchließen" data-dismiss="modal">Abbrechen</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal für Kundeneinstellungen -->

<div class="modal fade" id="myModalKundenEinstellungen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" >
                <button type="button" class="btn btn-primary glyphicon glyphicon-plus" id="kundenHinzufuegen" title="Kunden hinzufügen" ></button>
                <button type="button" class="close einstellungSchließen" data-dismiss="modal" aria-label="Close"><span
                        class="glyphicon glyphicon-remove" aria-hidden="true" title="Schließen"></span></button>
                <div id="myModalLabelKundenEinstellungenLabel">
                    <h4 class="modal-title" id="myModalLabelKundenEinstellungen">Kunden Menü</h4>

                </div>

            </div>

            <div class="modal-body" id="modalBodyKundenEinstellungen">

            </div>
            <div class="modal-footer">
                <button type="button" id="KundenEinstellungSchließen" class="btn btn-danger einstellungSchließen" title="Abbrechen" data-dismiss="modal">Abbrechen</button>
            </div>
        </div>
    </div>
</div>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>


</body>
</html>