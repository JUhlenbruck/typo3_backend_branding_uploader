
$(document).on('ready', function(){

    $.ajax({
        url: "http://host-2:8888/php/jsonApi.php",
        jsonp: "callback",
        dataType: "jsonp",
        success: function ( BildDataList ) {

            $.each( BildDataList , function (index, bildData) {

                var divTag = $('<div></div>');
                divTag.css({
                    "background-image": "url("+bildData.bildname+")"

                });
                $('#gallery').append(divTag);

                var interval = setInterval(function(){

                    $('#gallery div:last').fadeOut(1000, function(){
                        $('#gallery').prepend($(this));
                        $(this).show();
                    });
                }, 5000);
            });

        }
    });
 });
