/**
 * Created by januhlenbruck on 16.08.16.
 */

$(document).on('ready', function () {

    $.ajax({
        url: "http://host-2:8888/php/jsonApi.php",//http://typo3backend.cyclos-development3.de/php/jsonApi.php,
        jsonp: "callback",
        dataType: "jsonp",
        data: {
            kunde: "Jan"
        },
        success: function (BildDataList) {

            $.each(BildDataList, function (index, bildData) {

                var divTag = $('<div></div>');
                divTag.addClass('testClass' + bildData.position);
                divTag.css({
                    "background-image": "url(" + bildData.bildname + ")"

                });
                $('#gallery').append(divTag);

                var pTag = $('<p class="untenRechts"> </p>');
                pTag.text(bildData.beschreibung);

                $('.testClass' + bildData.position).append( pTag );

                var interval = setInterval(function () {

                    $('#gallery div:last').fadeOut(1000, function () {
                        $('#gallery').prepend($(this));
                        $(this).show();
                    });
                }, 5000);
            });

        }
    });
});
