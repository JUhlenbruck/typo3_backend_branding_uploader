/**
 * Created by januhlenbruck on 22.08.16.
 */
$(document).on('ready', function(){

    var testTag = $('<button type="button" title="Login verkleinern" class="tcon tcon-remove tcon-remove--chevron-down" id="loginCollapseTrigger" aria-label="remove item"><span class="tcon-visuallyhidden">remove item</span></button>');

    $(".typo3-login-wrap").append(testTag);


    $("#loginCollapseTrigger").on('click', function ( event ) {

        $('#loginCollapseTrigger').toggleClass('tcon-transform');
        $('.typo3-login-container').toggleClass('login-collapse');
        $('.untenRechts').toggleClass('login-collapse');
    });




});
